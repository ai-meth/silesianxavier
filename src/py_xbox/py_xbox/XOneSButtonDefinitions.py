from evdev import ecodes, InputDevice, ff, util
import time

class XPad():
    XPadButtons = {
        "A": 0,
        "B": 1,
        "X": 2,
        "Y": 3,
        "LB": 4,
        "RB": 5,
        "BACK": 6,
        "START": 7,
        "LSTICK": 8,
        "RSTICK": 9,
    }

    XPadAxes = {
        "SRX": 3,
        "SRY": 4,
        "SLX": 0,
        "SLY": 1,
        "TL": 2,
        "TR": 5,
        "HX": 6,
        "HY": 7,
    }

    def __init__(self) -> None:
        self.dev = None

        for name in util.list_devices():
            self.dev = InputDevice(name)
            if ecodes.EV_FF in self.dev.capabilities():
                break

        if self.dev is None:
            print("Sorry, no FF capable device found")
        else:
            print("Found " + self.dev.name + " at " + self.dev.path)

        self.rumble(500)

    def rumble(self, duration_ms = 1000):
        rumble = ff.Rumble(strong_magnitude=0xc000, weak_magnitude=0xc000)
        effect_type = ff.EffectType(ff_rumble_effect=rumble)

        effect = ff.Effect(
            ecodes.FF_RUMBLE, 
            -1,
            0, 
            ff.Trigger(0, 0), 
            ff.Replay(duration_ms, 0),
            ff.EffectType(ff_rumble_effect=rumble)
        )
            
        effect_id = self.dev.upload_effect(effect)
            
        repeat_count = 1
        
        self.dev.write(ecodes.EV_FF, effect_id, repeat_count)
        time.sleep(1)
        self.dev.erase_effect(effect_id) 
