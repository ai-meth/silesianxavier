import json
import serial


class ControllingDevice():
    def __init__(self, path_device: str = "/dev/ttyACM0", baud_rate: int = 9600,
                 timeout: float = 2.5):
        """
        :param path_device: Path to device controlling chassis
        :param baud_rate: Baud rate for chassis controlling device
        :param timeout: Timeout for chassis controlling device
        TODO sprawdzić czy to będzie banglać
        """
        try:
            self.controller = serial.Serial(path_device, baudrate=baud_rate, timeout=timeout)
            test_data = 'ACK'
            self.controller.write(test_data.encode('ascii'))
        except serial.serialutil.SerialException:
            print(f"[Errno 2] could not open port {path_device}: [Errno 2] No such file or directory: '{path_device}'")
            self.controller = None

    def isOpen(self):
        if self.controller is None:
            return False
        else:
            return self.controller.is_open


class Robot:
    """
    Class for manage Phoenix I platform in new Python3.8 style
    """

    def __init__(self, chassis_device: ControllingDevice = None, max_speed: list = None, max_speed_rotate: list = None,
                 speed: int = 0, speed_rotate: int = 0,
                 block: bool = True):
        """
        :param max_speed: list of max speed for chassis
        :param max_speed_rotate: list of max speed rotate for chassis
        :param speed: actual speed
        :param speed_rotate: actual rotate speed
        :param block: chassis software block status
        :param chassis_device: Device controlling chassis
        """

        if chassis_device is None:
            chassis_device = ControllingDevice()
        if max_speed is None:
            max_speed = [0, 30, 60, 90]
        if max_speed_rotate is None:
            max_speed_rotate = [0, 30, 50, 70]

        self.max_speed = max_speed
        self.max_speed_rotate = max_speed_rotate
        self.speed = speed
        self.speed_rotate = speed_rotate
        self.block = block
        self.chassis_device = chassis_device

    def chassis(self, velocity: float, turn_ratio: float) -> None:
        """
        Function using for controlling chassis

        :param velocity: Velocity value send to chassis controller
        :param turn_ratio: Turn ratio send to chassis controller
        """
        limit_work = 0
        limit_work2 = 2

        # Left and right motor enable status
        motor_left = 0
        motor_right = 0

        # Left and right motor PWM status
        left_motor_speed = 0
        right_motor_speed = 0

        # Function implemented in Python from old program, IDK how, but it works
        vel_abs = abs(velocity)
        vel_gen_abs = self.map(vel_abs, 0, 255, 0, self.speed)

        dir_abs = abs(turn_ratio)
        dir_gen_abs = self.map(dir_abs, 0, 255, 0, self.speed)

        speed_d = self.map(dir_abs, 0, 255, 0, vel_gen_abs)
        speed_v = self.map(vel_abs, 0, 255, 0, dir_gen_abs)

        if (velocity > limit_work) and (turn_ratio > limit_work) and (velocity >= turn_ratio):
            left_motor_speed = vel_gen_abs
            right_motor_speed = vel_gen_abs - speed_d
            motor_left = 1
            motor_right = 1
        elif (velocity >= limit_work) and (turn_ratio >= limit_work) and (velocity < turn_ratio):
            left_motor_speed = dir_gen_abs
            right_motor_speed = dir_gen_abs - speed_v
            motor_left = 0
            motor_right = 1
        elif (velocity < -limit_work) and (turn_ratio > limit_work) and (turn_ratio >= abs(velocity)):
            left_motor_speed = dir_gen_abs - speed_v
            right_motor_speed = dir_gen_abs
            motor_left = 0
            motor_right = 1
        elif (velocity <= -limit_work) and (turn_ratio >= limit_work) and (abs(velocity) > turn_ratio):
            left_motor_speed = vel_gen_abs - speed_d
            right_motor_speed = vel_gen_abs
            motor_left = 0
            motor_right = 0
        elif (velocity < -limit_work) and (turn_ratio < -limit_work) and (abs(velocity) >= abs(turn_ratio)):
            left_motor_speed = vel_gen_abs
            right_motor_speed = vel_gen_abs - speed_d
            motor_left = 0
            motor_right = 0
        elif (velocity <= -limit_work) and (turn_ratio <= -limit_work) and (abs(turn_ratio) > abs(velocity)):
            left_motor_speed = dir_gen_abs
            right_motor_speed = dir_gen_abs - speed_v
            motor_left = 1
            motor_right = 0
        elif (velocity > limit_work) and (turn_ratio < -limit_work) and (abs(turn_ratio) >= velocity):
            left_motor_speed = dir_gen_abs - speed_v
            right_motor_speed = dir_gen_abs
            motor_left = 1
            motor_right = 0
        elif (velocity >= limit_work) and (turn_ratio <= -limit_work) and (abs(velocity) > turn_ratio):
            left_motor_speed = vel_gen_abs - speed_d
            right_motor_speed = vel_gen_abs
            motor_left = 1
            motor_right = 1

        data = {"block": self.block}

        if (velocity >= -limit_work2) and (velocity <= limit_work2) and (turn_ratio >= -limit_work2) and (
                turn_ratio <= limit_work2):
            data["Motor_left_L_EN"] = 0
            data["Motor_left_R_EN"] = 0
            data["Motor_right_L_EN"] = 0
            data["Motor_right_R_EN"] = 0

            data["Motor_left_LPWM"] = 0
            data["Motor_left_RPWM"] = 0
            data["Motor_right_LPWM"] = 0
            data["Motor_right_RPWM"] = 0
        else:
            data["Motor_left_L_EN"] = 1
            data["Motor_left_R_EN"] = 1
            data["Motor_right_L_EN"] = 1
            data["Motor_right_R_EN"] = 1

        if motor_left:
            data["Motor_left_LPWM"] = int(left_motor_speed)
            data["Motor_left_RPWM"] = 0
        else:
            data["Motor_left_LPWM"] = 0
            data["Motor_left_RPWM"] = int(left_motor_speed)

        if motor_right:
            data["Motor_right_LPWM"] = int(right_motor_speed)
            data["Motor_right_RPWM"] = 0
        else:
            data["Motor_right_LPWM"] = 0
            data["Motor_right_RPWM"] = int(right_motor_speed)

        data = json.dumps(data)
        if self.chassis_device.controller.isOpen():
            self.chassis_device.write(data.encode('ascii'))
            self.chassis_device.flush()

    def chassis_stop(self):
        """
        Function to send STOP signal to controlling device
        """
        data = {"block": True, "Motor_left_L_EN": 0, "Motor_left_R_EN": 0, "Motor_right_L_EN": 0, "Motor_right_R_EN": 0,
                "Motor_left_LPWM": 0, "Motor_left_RPWM": 0, "Motor_right_LPWM": 0, "Motor_right_RPWM": 0}

        data = json.dumps(data)
        if self.chassis_device.isOpen():
            self.chassis_device.write(data.encode('ascii'))
            self.chassis_device.flush()

    @staticmethod
    def map(value: float, left_min: float, left_max: float, right_min: float, right_max: float) -> float:
        """
        Re-maps a number from one range to another. That is, a value of left_min would get mapped to left_max,
        a value of from right_min to right_max, values in-between to values in-between, etc.

        :param value: the number to map
        :param left_min: the lower bound of the value’s current range
        :param left_max: the upper bound of the value’s current range
        :param right_min: the lower bound of the value’s target range
        :param right_max: the upper bound of the value’s target range
        :return: The mapped value
        """
        left = left_max - left_min
        right = right_max - right_min

        value = float(value - left_min) / float(left)

        return right_min + (value * right)
