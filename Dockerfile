# syntax=docker/dockerfile:1

FROM nvidiajetson/l4t-ros2-foxy-pytorch:r32.5

SHELL ["/bin/bash", "-c"]

WORKDIR /ros_dev

# docker run -it -d --network host -v ~/ros2-foxy-docker:/ros_dev --name ros2-foxy nvidiajetson/l4t-ros2-foxy-pytorch:r32.5 bash